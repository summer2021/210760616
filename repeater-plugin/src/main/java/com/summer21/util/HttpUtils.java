package com.summer21.util;

import okhttp3.*;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author ZT 2021-07-25 15:25
 */
public class HttpUtils {
    private static final String METHOD_DELETE = "DELETE";
    private static final String METHOD_HEAD = "HEAD";
    private static final String METHOD_GET = "GET";
    private static final String METHOD_OPTIONS = "OPTIONS";
    private static final String METHOD_POST = "POST";
    private static final String METHOD_PUT = "PUT";
    private static final String METHOD_TRACE = "TRACE";

    private static final OkHttpClient client = new OkHttpClient().newBuilder()
            .connectTimeout(3, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build();


    public static Response doGet(String url, Map<String, String> headers, Map<String, String[]> params) throws Throwable {
        HttpUrl httpUrl = addQueryParameters(params, url);
        Request.Builder url1 = new Request.Builder()
                .get()
                .url(httpUrl);
        addHeaders(headers, url1);
        Call call = client.newCall(url1.build());
        return call.execute();
    }


    public static Response doPost(String url, Map<String, String> headers, Map<String, String[]> params, String body, String contentType) throws Throwable {
        // body
        RequestBody requestBody = RequestBody.create(MediaType.parse(contentType), body);
        // params
        HttpUrl httpUrl = addQueryParameters(params, url);
        Request.Builder url1 = new Request.Builder()
                .post(requestBody)
                .url(httpUrl);
        addHeaders(headers, url1);
        Call call = client.newCall(url1.build());
        return call.execute();
    }

    private static HttpUrl addQueryParameters(Map<String, String[]> params, String url) {
        HttpUrl.Builder builder = HttpUrl.parse(url).newBuilder();
        if (params != null) {
            for (Map.Entry<String, String[]> stringEntry : params.entrySet()) {
                for (String s : stringEntry.getValue()) {
                    builder.addQueryParameter(stringEntry.getKey(), s);
                }
            }
        }
        return builder.build();
    }

    private static void addHeaders(Map<String, String> headers, Request.Builder reqBuilder) {
        if (headers != null) {
            for (Map.Entry<String, String> headerEntry : headers.entrySet()) {
                reqBuilder.addHeader(headerEntry.getKey(), headerEntry.getValue());
            }
        }
    }


    public static Response executeRequest(String url, String method, Map<String, String> headers, Map<String, String[]> params, String body, String contentType) throws Throwable {
        if (METHOD_GET.equals(method)) {
            return doGet(url, headers, params);
        } else if (method.equals(METHOD_HEAD)) {

        } else if (method.equals(METHOD_POST)) {
            return doPost(url, headers, params, body, contentType);
        } else if (method.equals(METHOD_PUT)) {

        } else if (method.equals(METHOD_DELETE)) {

        } else if (method.equals(METHOD_OPTIONS)) {

        } else if (method.equals(METHOD_TRACE)) {

        } else {
            //
            // Note that this means NO servlet supports whatever
            // method was requested, anywhere on this server.
            // "http.method_not_implemented"
        }
        return null;
    }
}
