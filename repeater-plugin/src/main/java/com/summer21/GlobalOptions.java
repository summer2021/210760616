package com.summer21;


/**
 * 全局开关
 * Created by vlinux on 15/6/4.
 */
public class GlobalOptions {

    /**
     * 是否支持系统类<br/>
     * 这个开关打开之后将能代理到来自JVM的部分类，由于有非常强的安全风险可能会引起系统崩溃<br/>
     * 所以这个开关默认是关闭的，除非你非常了解你要做什么，否则请不要打开
     */
    public static volatile boolean isUnsafe = false;

    /**
     * 是否支持dump被增强的类<br/>
     * 这个开关打开这后，每次增强类的时候都将会将增强的类dump到文件中，以便于进行反编译分析
     */
    public static volatile boolean isDump = false;

    /**
     * 是否支持批量增强<br/>
     * 这个开关打开后，每次均是批量增强类
     */
    public static volatile boolean isBatchReTransform = true;

    /**
     * 是否支持json格式化输出<br/>
     * 这个开关打开后，使用json格式输出目标对象，配合-x参数使用
     */
    public static volatile boolean isUsingJson = false;

    /**
     * 是否关闭子类
     */
    public static volatile boolean isDisableSubClass = false;

    /**
     * 是否在interface类里搜索函数
     * https://github.com/alibaba/arthas/issues/1105
     */
    public static volatile boolean isSupportDefaultMethod = true /*&& JavaVersionUtils.isGreaterThanJava7()*/;

    /**
     * 是否日志中保存命令执行结果
     */
    public static volatile boolean isSaveResult = false;

    /**
     * job的超时时间
     */
    public static volatile String jobTimeout = "1d";

    /**
     * 是否打印parent类里的field
     * @see com.taobao.arthas.core.view.ObjectView
     */
    public static volatile boolean printParentFields = true;

    /**
     * 是否打开verbose 开关
     */
    public static volatile boolean verbose = false;

    public static volatile String trafficPath = "~/.traffic/";
}
