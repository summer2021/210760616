package com.summer21.system;

import com.alibaba.oneagent.plugin.Plugin;

/**
 * 
 * @author hengyunabc 2020-05-18
 *
 */
public interface PluginAware {

    public Plugin getPlugin();

    public void setPlugin(Plugin plugin);

}
