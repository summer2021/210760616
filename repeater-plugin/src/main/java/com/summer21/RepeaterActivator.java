package com.summer21;

import com.alibaba.oneagent.plugin.Plugin;
import com.alibaba.oneagent.plugin.PluginActivator;
import com.alibaba.oneagent.plugin.PluginContext;
import com.summer21.core.advisor.TransformerManager;
import com.summer21.core.repeater.ClassLoaderMap;
import com.summer21.core.repeater.RepeaterCommand;
import com.summer21.util.StringUtils;

import java.io.File;
import java.lang.instrument.Instrumentation;
import java.security.CodeSource;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.jar.JarFile;

/**
 * ArthasBootstrap session cookie 网关 流量 网关 不是http直接暴露在外
 * dom 流量回放 阿里云
 *
 * @author ZT 2021-07-23 22:45
 */
public class RepeaterActivator implements PluginActivator {

    private static RepeaterActivator repeaterActivator;

    private static final String REPEATER_SPY_JAR = "spy-lib/repeater-spy.jar";

    private Instrumentation instrumentation;

    private TransformerManager transformerManager;
    private ScheduledExecutorService executorService;

    private Plugin plugin;


    @Override
    public boolean enabled(PluginContext context) {
        if (repeaterActivator == null) {
            repeaterActivator = this;
        }

        return "true".equals(context.getProperty("repeater.enable"));
    }

    @Override
    public void init(PluginContext context) throws Exception {
        Instrumentation instrumentation = context.getInstrumentation();
        this.instrumentation = instrumentation;
        this.plugin = context.getPlugin();

        // 1. initSpy()
        try {
            initSpy();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        // 2. init listener


        executorService = Executors.newScheduledThreadPool(1, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                final Thread t = new Thread(r, "arthas-command-execute");
                t.setDaemon(true);
                return t;
            }
        });

        // init classloader cache
        ClassLoaderMap.init(instrumentation);

        transformerManager = new TransformerManager(instrumentation);
        System.out.println("RepeaterActivator init");
    }

    @Override
    public void start(final PluginContext context) throws Exception {
        String trafficPath = context.getProperty("repeater.trafficPath");
        if (!StringUtils.isEmpty(trafficPath)) {
            GlobalOptions.trafficPath = trafficPath;
        }
        final String needRepeat = context.getProperty("repeater.needRepeat");
        Thread repeaterActivatorStart = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    // 增强类的时候需要类已加载，所以...
                    // 或者如何判断目标程序时候已经启动，或者以attach的形式启动
                    // 或者更改Enhance#enhance的finally，然后以另外的方式结束transformer
                    Thread.sleep(10 * 1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("RepeaterActivator start");
                // 只要启动插件，就会进行流量录制
                record(context);
                // 回放需要根据指令来确定是否执行
                if ("true".equals(needRepeat)) {
                    repeat();
                }
            }
        });
        repeaterActivatorStart.setDaemon(true);
        repeaterActivatorStart.start();
    }

    /**
     * 开始录制
     *
     * @param context
     */
    private void record(PluginContext context) {
        String subInvoke = context.getProperty("repeater.subInvoke");
        RepeaterCommand repeaterCommand = new RepeaterCommand();
        repeaterCommand.setRecorder(context.getProperty("repeater.pluginName"));
        repeaterCommand.setSubInvoke(subInvoke);
        repeaterCommand.process(plugin);
    }

    private void repeat() {
        RepeaterCommand repeaterCommand = new RepeaterCommand();
        repeaterCommand.setPlay(true);
        // index暂时没用
        repeaterCommand.setIndex(1000);
        repeaterCommand.process(plugin);
    }

    @Override
    public void stop(PluginContext context) throws Exception {
        // equivalent to shutdown hooks stop Runtime.getRuntime().addShutdownHook(shutdown);
        System.out.println("RepeaterActivator stop");
    }


    /**
     * @throws Throwable
     * @see com.summer21.core.advisor.Enhancer#transform
     * 使用SpyAPI.class时，需要当中的inClassLoader能加载到，所以要提前将Spy添加到BootstrapClassLoader
     */
    private void initSpy() throws Throwable {
        // 将Spy添加到BootstrapClassLoader
        ClassLoader parent = ClassLoader.getSystemClassLoader().getParent();
        Class<?> spyClass = null;
        if (parent != null) {
            try {
                spyClass = parent.loadClass("java.repeater.SpyAPI");
            } catch (Throwable e) {
                // ignore
            }
        }
        // TODO 路径更改以及生成spy jar
        if (spyClass == null) {
            CodeSource codeSource = RepeaterActivator.class.getProtectionDomain().getCodeSource();
            if (codeSource != null) {
                File arthasCoreJarFile = new File(codeSource.getLocation().toURI().getSchemeSpecificPart());
                File spyJarFile = new File(arthasCoreJarFile.getParentFile(), REPEATER_SPY_JAR);
                instrumentation.appendToBootstrapClassLoaderSearch(new JarFile(spyJarFile));
            } else {
                throw new IllegalStateException("can not find " + REPEATER_SPY_JAR);
            }
        }
    }


    /**
     * @return RepeaterActivator单例
     */
    public static RepeaterActivator getInstance() {
        if (repeaterActivator == null) {
            throw new IllegalStateException("ArthasBootstrap must be initialized before!");
        }
        return repeaterActivator;
    }

    public ScheduledExecutorService getScheduledExecutorService() {
        return this.executorService;
    }

    public Instrumentation getInstrumentation() {
        return instrumentation;
    }

    public TransformerManager getTransformerManager() {
        return transformerManager;
    }

    public Plugin getPlugin() {
        return plugin;
    }
}
