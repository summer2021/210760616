package com.summer21.core.repeater.servlet;

import com.summer21.core.repeater.InvocationContext;

import java.util.Map;

/**
 * @author ZT 2021-07-25 14:40
 */
public class ServletInvocationContextWrapper {

    private static final String URL = "URL";
    private static final String METHOD = "METHOD";
    private static final String HEADERS = "HEADERS";
    private static final String PARAMETER_MAP = "PARAMETER_MAP";
    private static final String BODY = "BODY";
    private static final String CONTENT_TYPE = "CONTENT_TYPE";
    private static final String RESPONSE = "RESPONSE";

    public ServletInvocationContextWrapper(InvocationContext invocationContext) {
        this.invocationContext = invocationContext;
    }

    InvocationContext invocationContext;

    public String getContentType() {
        return (String) invocationContext.get(CONTENT_TYPE);
    }

    public void setContentType(String contentType) {
        invocationContext.put(CONTENT_TYPE, contentType);
    }

    public String getRequestURL() {
        return (String) invocationContext.get(URL);
    }

    public void setRequestURL(String requestURL) {
        invocationContext.put(URL, requestURL);
    }

    public String getMethod() {
        return (String) invocationContext.get(METHOD);
    }

    public void setMethod(String method) {
        invocationContext.put(METHOD, method);
    }

    public Map<String, String> getHeaders() {
        return (Map<String, String>) invocationContext.get(HEADERS);
    }

    public void setHeaders(Map<String, String> headers) {
        invocationContext.put(HEADERS, headers);
    }

    public Map<String, String[]> getParameterMap() {
        return (Map<String, String[]>) invocationContext.get(PARAMETER_MAP);
    }

    public void setParameterMap(Map<String, String[]> parameterMap) {
        invocationContext.put(PARAMETER_MAP, parameterMap);
    }

    public String getBody() {
        return (String) invocationContext.get(BODY);
    }

    public void setBody(String body) {
        invocationContext.put(BODY, body);
    }

    public void setResponse(byte[] response) {
        invocationContext.put(RESPONSE, response);
    }

    public byte[] getResponse() {
        return (byte[]) invocationContext.get(RESPONSE);
    }
}
