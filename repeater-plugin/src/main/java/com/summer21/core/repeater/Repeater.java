package com.summer21.core.repeater;

import com.summer21.core.advisor.ArthasMethod;
import com.summer21.util.matcher.Matcher;

/**
 * todo 入口调用和子调用的matcher用Matcher OR
 * TODO 执行回放的时候，如果要Mock子调用，就要调用enhance，才能调用listener中的方法。因为在录制完毕之后，该listener会被自动清除。
 *
 * @author ZT 2021-07-09 22:29
 */
public interface Repeater {


    /**
     * 类名匹配
     *
     * @return 获取类名匹配
     */
    Matcher getClassNameMatcher();

    /**
     * 排除类名匹配
     *
     * @return
     */
    Matcher getClassNameExcludeMatcher();

    /**
     * 方法名匹配
     *
     * @return 获取方法名匹配
     */
    Matcher getMethodNameMatcher();


    /**
     * 执行回放。
     * 1. 如果要mock子调用就要进行enhance
     * 2. 还应该加上一个入参，回放选项。
     *
     * @param repeaterTraffic 通知点
     */
    void repeat(RepeaterTraffic repeaterTraffic) throws Throwable;


    /**
     * 根据命令
     *
     * @return 插件名称
     */
    String getTrafficType();

    /**
     * 是否为入口流量调用的方法
     * @param method ArthasMethod
     * @return true / false
     */
    boolean isTrafficMethod(ArthasMethod method);

    /**
     * 是否为重载方法.
     * 方法增强时未区分是否为重载方法。所以录制时需要判断
     * @return true/false
     */
    boolean isOverloadMethod(ArthasMethod arthasMethod);

    /**
     * 做好录制准备
     * @param loader 流量入口类的加载器
     * @param clazz 流量入口类
     * @param method ArthasMethod
     * @param target 类实例
     * @param args 方法入参
     */
    void before(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args) throws Throwable;

    /**
     * 入口调用返回
     * @param returnObject 返回值
     */
    void afterReturning(Object returnObject);

    /**
     * 入口调用异常
     * @param throwable 异常
     */
    void afterThrowing(Throwable throwable);


}
