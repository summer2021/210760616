package com.summer21.core.repeater;

import java.util.Date;

/**
 * 一次调用的网络流量。序列化保存的就是这玩意儿
 */
public class RepeaterTraffic implements java.io.Serializable {

    private RepeaterRecord repeaterRecord;
    private Date gmtCreate;
    private double cost;
    private String trafficType;
    private String seq;

    /**
     * 如果没有默认构造方法，反序列化时可能会出错，fastJson
     */
    private RepeaterTraffic(){}

    public RepeaterTraffic(RepeaterRecord repeaterRecord, Date gmtCreate, double cost, String trafficType) {
        this.repeaterRecord = repeaterRecord;
        this.gmtCreate = gmtCreate;
        this.cost = cost;
        this.trafficType = trafficType;
    }


    public RepeaterRecord getRepeaterRecord() {
        return repeaterRecord;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public double getCost() {
        return cost;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getTrafficType() {
        return trafficType;
    }

    public void setRepeaterRecord(RepeaterRecord repeaterRecord) {
        this.repeaterRecord = repeaterRecord;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setTrafficType(String trafficType) {
        this.trafficType = trafficType;
    }
}
