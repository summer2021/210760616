package com.summer21.core.repeater;

import com.alibaba.oneagent.plugin.Plugin;
import com.summer21.core.advisor.AdviceListenerAdapter;
import com.summer21.core.advisor.ArthasMethod;
import com.summer21.util.ArthasCheckUtils;
import com.summer21.util.ThreadLocalWatch;

import java.util.Date;
import java.util.Map;

/**
 * 相比于tt，没有考虑递归的问题
 * 1. 处理录制的逻辑，所有调用都要判断是否为重放，如果是重放就不进行记录。
 * 2. 由于不是继承的AbstractTraceAdviceListener，所以不会跳过JDK相关的子调用。
 * 3. TODO 鉴于arthas使用了netty，classLoader要注意，如果是arthas的classLoader就直接跳过。
 * 但在arthas中类增强时已经排除了arthas classloader加载的类。所以在本插件中，要做的是，排除由其他插件的类加载器加载的类。
 * 5. TODO 监听完之后，需要将Listener从Manager中去除。实际上涉及到扫尾工作的还有enhancer这个transformer，arthas中将其与process生命周期绑定在一起。
 * 但是在现在这个插件当中，transformer和listener都是和plugin的生命周期绑定在一起，只有plugin挂了前两者才挂。
 *
 * @author ZT
 * @version 1.0
 */
public class RepeaterAdviceListener extends AdviceListenerAdapter {


    /**
     * 记录当前线程的流量记录。因为流量可以并发。
     * 没有初始化。就是null。
     */
    public static ThreadLocal<RepeaterRecord> repeaterRecordRef = new ThreadLocal<RepeaterRecord>();

    /**
     * 回放时用来记录所回放流量的ID
     */
    public static ThreadLocal<String> recordIdRef = new ThreadLocal<String>();

    private final ThreadLocal<ObjectStack> argsRef = new ThreadLocal<ObjectStack>() {
        @Override
        protected ObjectStack initialValue() {
            return new ObjectStack(512);
        }
    };


    private final Repeater repeater;

    /**
     * 方法执行时间戳
     */
    private final ThreadLocalWatch threadLocalWatch = new ThreadLocalWatch();

    private final RepeaterCommand command;


    public RepeaterAdviceListener(RepeaterCommand command, Plugin plugin, boolean verbose, Repeater repeater) {
        this.command = command;
        setPlugin(plugin);
//        this.plugin = plugin;
        super.setVerbose(verbose);
        this.repeater = repeater;
    }

    private boolean isRecorder(){
        return repeaterRecordRef.get() != null;
    }

    private boolean isRepeatFlow(){
        String recordId = recordIdRef.get();
        return AbstractRepeater.isRepeatFlow(recordId);
    }

    private Object deepCopy(Object o){
        byte[] serialize = RepeaterCommand.serializer.serialize(o);
        return RepeaterCommand.serializer.deserialize(serialize);
    }

    /**
     *
     */
    @Override
    public void before(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args)
            throws Throwable {
        // 入口调用，提取流量信息
        if (repeater.isTrafficMethod(method)) {
            if (repeater.isOverloadMethod(method)) {
                return;
            }
            repeater.before(loader, clazz, method, target, args);
            threadLocalWatch.start();
            return;
        }
        if(isRecorder()){
            // deep copy args
            Object[] objects = new Object[args.length];
            for (int i = 0; i < args.length; i++) {
                Object arg = args[i];
                objects[i] = deepCopy(arg);
            }
            argsRef.get().push(objects);
        } else if(isRepeatFlow()) {
            // TODO mock返回 继续解决如何返回子调用的问题
            // 获取记录中的子调用结果
            RepeaterTraffic repeaterTraffic = command.getRepeaterTraffic(recordIdRef.get());
            Map<String, SubInvocation> subInvocations = repeaterTraffic.getRepeaterRecord().getSubInvocations();
            String key = SubInvocation.key(clazz.getName(), method.getName(), method.getDesc());
            SubInvocation subInvocation = subInvocations.get(key);

        }
    }

    private void afterFinishing() {
        double cost = threadLocalWatch.costInMillis();
        if (isRepeatFlow()) {
            // TODO 流量重放完毕，可以考虑做结果对比

            recordIdRef.remove();
            return;
        }

        RepeaterRecord repeaterRecord = repeaterRecordRef.get();
        repeaterRecordRef.remove();
        argsRef.remove();
        RepeaterTraffic repeaterTraffic = new RepeaterTraffic(repeaterRecord, new Date(), cost, repeater.getTrafficType());
        String id = command.putRepeaterTraffic(repeaterTraffic);
        repeaterTraffic.setSeq(id);
        // 保存和序列化
        command.saveTraffic(repeaterTraffic);
        // 限制录制次数
//        plugin.times().incrementAndGet();
//        if (isLimitExceeded(command.getNumberOfLimit(), plugin.times().get())) {
//            abortProcess(plugin, command.getNumberOfLimit());
//        }
    }

    @Override
    public void afterReturning(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args,
                               Object returnObject) throws Throwable {
        if (repeater.isTrafficMethod(method)) {
            if (repeater.isOverloadMethod(method)) {
                return;
            }
            repeater.afterReturning(returnObject);
            afterFinishing();
            return;
        }
        if(isRecorder()){
            // deep copy returnObject
            Object copy = deepCopy(returnObject);
            Object pop = argsRef.get().pop();
            SubInvocation subInvocation = SubInvocation.newForAfterRetuning((Object[]) pop, copy);
            // 存储入参和结果
            RepeaterRecord repeaterRecord = repeaterRecordRef.get();
            String key = SubInvocation.key(clazz.getName(), method.getName(), method.getDesc());
            repeaterRecord.getSubInvocations().put(key, subInvocation);
        }else if(isRepeatFlow()) {
            // TODO mock返回
        }
    }

    @Override
    public void afterThrowing(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args,
                              Throwable throwable) throws Throwable {
        if (repeater.isTrafficMethod(method)) {
            if (repeater.isOverloadMethod(method)) {
                return;
            }
            repeater.afterThrowing(throwable);
            afterFinishing();
            return;
        }
        if(isRecorder()){
            // deep copy throwable
            Throwable copy = (Throwable) deepCopy(throwable);
            Object pop = argsRef.get().pop();
            SubInvocation subInvocation = SubInvocation.newForAfterThrowing((Object[]) pop, copy);
            // 存储入参和结果
            RepeaterRecord repeaterRecord = repeaterRecordRef.get();
            String key = SubInvocation.key(clazz.getName(), method.getName(), method.getDesc());
            repeaterRecord.getSubInvocations().put(key, subInvocation);
        }else if(isRepeatFlow()) {
            // TODO mock返回
        }
    }


    /**
     *
     * <pre>
     * 一个特殊的stack，为了追求效率，避免扩容。
     * 因为这个stack的push/pop 并不一定成对调用，比如可能push执行了，但是后面的流程被中断了，pop没有被执行。
     * 如果不固定大小，一直增长的话，极端情况下可能应用有内存问题。
     * 如果到达容量，pos会重置，循环存储数据。所以使用这个Stack如果在极端情况下统计的数据会不准确，只用于monitor/watch等命令的计时。
     *
     * </pre>
     *
     * @author hengyunabc 2020-05-20
     *
     */
    static class ObjectStack {
        private Object[] array;
        private int pos = 0;
        private int cap;

        public ObjectStack(int maxSize) {
            array = new Object[maxSize];
            cap = array.length;
        }

        public int size() {
            return pos;
        }

        public void push(Object value) {
            if (pos < cap) {
                array[pos++] = value;
            } else {
                // if array is full, reset pos
                pos = 0;
                array[pos++] = value;
            }
        }

        public Object pop() {
            if (pos > 0) {
                pos--;
                Object object = array[pos];
                array[pos] = null;
                return object;
            } else {
                pos = cap;
                pos--;
                Object object = array[pos];
                array[pos] = null;
                return object;
            }
        }
    }
}
