package com.summer21.core.repeater;

import com.alibaba.deps.org.objectweb.asm.Type;
import com.summer21.core.advisor.ArthasMethod;
import com.summer21.util.ArthasCheckUtils;
import com.summer21.util.SearchUtils;
import com.summer21.util.StringUtils;
import com.summer21.util.matcher.Matcher;

/**
 * 不同的框架有不同的流量入口，使用不同的插件。
 * 所有插件都从这里开始。
 * <p>
 * <p>
 *
 * @author ZT
 * @version 1.0
 */
public abstract class AbstractRepeater implements Repeater {

    protected abstract String getClassPattern();

    protected abstract String getExcludeClassPattern();

    protected abstract String getMethodPattern();

    /**
     * 获取被增强方法的参数类型名，用于判断是否为重载方法
     * 不直接使用Class是为了防止跨类加载器时加载不到类
     *
     * @return 返回被增强方法的参数类型名
     */
    protected abstract String[] getMethodArgumentsPatterns();

    /**
     * 类名匹配
     *
     * @return 获取类名匹配
     */
    @Override
    public Matcher getClassNameMatcher() {
        if (getClassPattern() != null) {
            return SearchUtils.classNameMatcher(getClassPattern(), false);
        }
        return null;
    }

    /**
     * 排除类名匹配
     *
     * @return 获取排除类名匹配
     */
    @Override
    public Matcher getClassNameExcludeMatcher() {
        if (getExcludeClassPattern() != null) {
            return SearchUtils.classNameMatcher(getExcludeClassPattern(), false);
        }
        return null;
    }

    /**
     * 方法名匹配
     *
     * @return 获取方法名匹配
     */
    @Override
    public Matcher getMethodNameMatcher() {
        if (getMethodPattern() != null) {
            return SearchUtils.classNameMatcher(getMethodPattern(), false);
        }
        return null;
    }


    /**
     * 提取出入流量的关键信息，如URL、body等等。
     *
     * @param repeaterRecord 通知点
     * @return 返回需要序列化保存的入口流量的关键信息
     */
    protected abstract InvocationContext extractContext(RepeaterRecord repeaterRecord);

    /**
     * 根据流量判断是否为回放流量，如果是回放流量则提取所属流量记录的ID
     * 不同框架不同有不同的参数设置方法，也就对应有不同的提取方法
     *
     * @param args 流量入口参数
     * @return 流量记录ID
     */
    protected abstract String extractRecordId(Object[] args);

    /**
     * 拦截流量，复制参数。
     * <p>
     * 如有必要，替换或wrapper args中的参数。就目前所知servlet需要wrapper。
     *
     * @param loader 类加载器
     * @param clazz  类
     * @param method 方法
     * @param target 目标类实例 若目标为静态方法,则为null
     * @param args   参数列表
     * @throws Throwable 通知过程出错
     */
    protected abstract void trafficWrapper(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args)
            throws Throwable;

    /**
     * 流量请求匹配
     *
     * @return 获取流量请求匹配
     */
    protected abstract Matcher getRequestMatcher();

    @Override
    public void before(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args) throws Throwable {
        // 流量过滤
        Matcher requestMatcher = getRequestMatcher();
        if (requestMatcher != null && requestMatcher.matching(args)) {
            RepeaterAdviceListener.repeaterRecordRef.set(null);
            return;
        }
        // 回放的流量
        String recordId = this.extractRecordId(args);
        if (isRepeatFlow(recordId)) {
            RepeaterAdviceListener.recordIdRef.set(recordId);
            return;
        }
        // 截获流量
        trafficWrapper(loader, clazz, method, target, args);
        RepeaterRecord repeaterRecord = new RepeaterRecord(loader, args);
        RepeaterAdviceListener.repeaterRecordRef.set(repeaterRecord);
    }

    @Override
    public void afterReturning(Object returnObject) {
        // 如果是被过滤流量，不会有记录
        RepeaterRecord repeaterRecord = RepeaterAdviceListener.repeaterRecordRef.get();
        if (repeaterRecord == null) {
            return;
        }
        repeaterRecord.setReturnObj(resolveReturnObject(returnObject));
        afterFinishing();
    }

    @Override
    public void afterThrowing(Throwable throwable) {
        // 如果是被过滤流量，不会有记录
        RepeaterRecord repeaterRecord = RepeaterAdviceListener.repeaterRecordRef.get();
        if (repeaterRecord == null) {
            return;
        }
        repeaterRecord.setThrowExp(resolveThrowable(throwable));
        afterFinishing();
    }

    protected Object resolveReturnObject(Object returnObject){
        return returnObject;
    }

    protected Throwable resolveThrowable(Throwable throwable){
        return throwable;
    }

    public static boolean isRepeatFlow(String recordId) {
        return !StringUtils.isEmpty(recordId);
    }

    private void afterFinishing() {
        if (isRepeatFlow(RepeaterAdviceListener.recordIdRef.get())) {
            return;
        }
        RepeaterRecord repeaterRecord = RepeaterAdviceListener.repeaterRecordRef.get();

        // 提取URL等信息，保存等
        InvocationContext invocationContext = extractContext(repeaterRecord);
        repeaterRecord.setInvocationContext(invocationContext);
    }

    @Override
    public final boolean isTrafficMethod(ArthasMethod method) {
        String name = method.getName();
        String targetClassName = method.getTargetClassName();
        return ArthasCheckUtils.isEquals(name, getMethodPattern()) && ArthasCheckUtils.isEquals(targetClassName, getClassPattern());
    }

    @Override
    public final boolean isOverloadMethod(ArthasMethod method) {
        String desc = method.getDesc();
        Type[] argumentTypes = Type.getArgumentTypes(desc);
        String[] methodArgumentsPatterns = getMethodArgumentsPatterns();
        if (argumentTypes.length != methodArgumentsPatterns.length) {
            return true;
        }
        for (int i = 0; i < methodArgumentsPatterns.length; i++) {
            String methodArgumentsPattern = methodArgumentsPatterns[i];
            String className = argumentTypes[i].getClassName();
            // 参数不相同就是重载方法
            if (!ArthasCheckUtils.isEquals(methodArgumentsPattern, className)) {
                return true;
            }
        }
        return false;
    }
}
