package com.summer21.core.repeater;

import java.lang.instrument.Instrumentation;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;

/**
 * 缓存当前JVM所有类加载器
 * TODO 更新map
 * @author ZT 2021-08-13 12:33
 */
public class ClassLoaderMap {

    private static final Map<String, ClassLoader> map = new HashMap<String, ClassLoader>();

    private static final String BOOTSTRAP_CLASSLOADER = "BOOTSTRAP_CLASSLOADER";

    /**
     * 出现问题。 PluginClassLoader存在多个实例，不同实例之间的URL都不一样，也就在A中无法加载B中的类
     * 所以如果是URLClassLoader，需要多两个属性来确定唯一，或者更多属性。或者直接用hash  摘要
     * @param classLoader classLoader
     * @return classLoader的名字
     */
    public static String key(ClassLoader classLoader){
        String name = null;
        if(classLoader != null) {
            name = classLoader.getClass().getName();
            if(classLoader instanceof URLClassLoader && classLoader.getParent() != null){
                URLClassLoader urlClassLoader = (URLClassLoader) classLoader;
                URL[] urls = urlClassLoader.getURLs();
                if (urls.length > 0) {
                    String firstFile = urls[0].toString();
                    name = name + "|" + firstFile;
                }
            }
        } else {
            name = BOOTSTRAP_CLASSLOADER;
        }
        return name;
    }

    public static void init(Instrumentation instrumentation){
        Class[] allLoadedClasses = instrumentation.getAllLoadedClasses();
        for (Class allLoadedClass : allLoadedClasses) {
            ClassLoader classLoader = allLoadedClass.getClassLoader();
            map.put(key(classLoader), classLoader);
        }
    }

    public static ClassLoader get(String classLoaderName) {
        // 如果是引导类加载器则返回扩展类加载器即可
        if(BOOTSTRAP_CLASSLOADER.equals(classLoaderName)){
            String extClassLoaderName = "sun.misc.Launcher$ExtClassLoader";
            return map.get(extClassLoaderName);
        }
        return map.get(classLoaderName);
    }

}
