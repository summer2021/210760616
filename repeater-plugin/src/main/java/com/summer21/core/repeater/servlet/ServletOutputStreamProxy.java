package com.summer21.core.repeater.servlet;

import com.summer21.util.reflect.ReflectUtils;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.repeater.BytesCopier;

/**
 * TODO 或许有更好的invoke方法？而不用挨个对比参数.  ServletOutputStream的子类的方法，是否会有影响？
 * https://stackoverflow.com/questions/8933054/how-to-read-and-copy-the-http-servlet-response-output-stream-content-for-logging/8972088#8972088?newreg=01a08577b7784972bd003a1be0650c56
 * @author ZT 2021-07-18 17:06
 */
public class ServletOutputStreamProxy implements MethodHandler {

    private static final String TARGET_CLASS_NAME = "javax.servlet.ServletOutputStream";

    private OutputStream originServletOutputStream;
    private ByteArrayOutputStream copy;


    private ServletOutputStreamProxy(OutputStream originServletOutputStream) {
        this.originServletOutputStream = originServletOutputStream;
        this.copy = new ByteArrayOutputStream(1024);
    }

    public static Object newServletOutputStreamProxy(final ClassLoader loader, OutputStream originServletOutputStream) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {

        ProxyFactory.classLoaderProvider = new ProxyFactory.ClassLoaderProvider() {
            @Override
            public ClassLoader get(ProxyFactory pf) {
                return ServletOutputStreamProxy.class.getClassLoader();
            }
        };
        ProxyFactory factory = new ProxyFactory();
        factory.setSuperclass(loader.loadClass(TARGET_CLASS_NAME));
        factory.setInterfaces(new Class[]{BytesCopier.class});
        ServletOutputStreamProxy servletOutputStreamProxy = new ServletOutputStreamProxy(originServletOutputStream);
        return factory.create(new Class<?>[0], new Object[0], servletOutputStreamProxy);
    }

    public void write(int b) throws IOException {
        originServletOutputStream.write(b);
        copy.write(b);
    }



    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }


    public void write(byte[] b, int off, int len) throws IOException {
        originServletOutputStream.write(b, off, len);
        copy.write(b, off, len);
    }


    public byte[] getCopy() {
        return copy.toByteArray();
    }


    @Override
    public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args)throws Throwable {
        String name = thisMethod.getName();
        Class<?>[] parameterTypes = thisMethod.getParameterTypes();
        if ("write".equals(name)
                && parameterTypes.length == 1 && parameterTypes[0].equals(int.class)) {
            write((int)args[0]);
            return null;
        } else if("write".equals(name)
                && parameterTypes.length == 3
                && parameterTypes[0].equals(byte[].class)
                && parameterTypes[1].equals(int.class)
                && parameterTypes[2].equals(int.class)){
            write((byte[])args[0], (int)args[1], (int)args[2]);
        } if("getCopy".equals(name) && parameterTypes.length == 0) {
            return getCopy();
        }
        return ReflectUtils.invoke(originServletOutputStream,thisMethod.getName(), thisMethod.getParameterTypes(), args);
    }
}