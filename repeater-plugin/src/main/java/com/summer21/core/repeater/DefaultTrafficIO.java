package com.summer21.core.repeater;


import com.summer21.GlobalOptions;
import com.summer21.core.serialize.Serializer;
import com.summer21.util.FileUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * 网络传输先不考虑
 * TODO read的逻辑
 *  异步读写的逻辑
 * @author ZT 2021-07-14 0:49
 */
public class DefaultTrafficIO implements TrafficIO {

    private static final String DEFAULT_DIR = GlobalOptions.trafficPath;
    
    Serializer serializer;

    public DefaultTrafficIO(Serializer serializer) {
        this.serializer = serializer;
    }

    /**
     * 暂时用时间戳+序列号+流量类型+类加载器名
     * @param repeaterTraffic 记录的一次流量调用
     * @return 文件名
     */
    protected String encodeFileName(RepeaterTraffic repeaterTraffic) {
        ClassLoader loader = repeaterTraffic.getRepeaterRecord().getLoader();
        String classLoaderName = "null";
        if (loader != null) {
            classLoaderName = loader.getClass().getSimpleName();
        }
        String seq = repeaterTraffic.getSeq();
        String trafficTypeName = repeaterTraffic.getTrafficType();
        return seq + "_" + trafficTypeName + "_" + classLoaderName;
    }

    @Override
    public void writeTraffic(RepeaterTraffic repeaterTraffic) throws IOException {
        String name = encodeFileName(repeaterTraffic);
        File dir = new File(DEFAULT_DIR);
        if (!dir.exists()){
            boolean ok = dir.mkdirs();
        }
        File file = new File(dir, name);
        byte[] serialize = serializer.serialize(repeaterTraffic);
        FileUtils.writeByteArrayToFile(file, serialize);
    }

    @Override
    public RepeaterTraffic readTraffic() {
        return null;
    }

    @Override
    public RepeaterTraffic[] readAllTraffic(){
        File dir = new File(DEFAULT_DIR);
        if(!dir.exists()) {
            return null;
        }
        File[] files = dir.listFiles();
        if(files == null){
            return null;
        }
        RepeaterTraffic[] rts = new RepeaterTraffic[files.length];
        int index = 0;
        for (File file : files) {
            try {
                byte[] bytes = FileUtils.readFileToBytes(file);
                rts[index++] = (RepeaterTraffic) serializer.deserialize(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rts;
    }

    @Override
    public void asynWriteTraffic(RepeaterTraffic repeaterTraffic) {

    }

    @Override
    public RepeaterTraffic[] asynReadTraffic() {
        return null;
    }
}
