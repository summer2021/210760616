package com.summer21.core.repeater;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ZT 2021-07-25 14:22
 */
public class InvocationContext implements java.io.Serializable {
    private Map<String, Object> map = new HashMap<>();

    public InvocationContext() {
    }

    public InvocationContext(Map<String, Object> map) {
        this.map = map;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public Object get(String key) {
        return map.get(key);
    }

    public void put(String key, Object value) {
        map.put(key, value);
    }
}
