package com.summer21.core.repeater.dubbo;

/**
 * @author ZT 2021-09-09 9:55
 */
public class DubboConsumerRepeater extends DubboProviderRepeater{
    public static final String PLUGIN_NAME = "DubboConsumer";

    /**
     * AbstractInvoker只在consumer侧
     * DubboInvoker或者InjvmInvoker或者TripleInvoker都继承自AbstractInvoker
     * 在服务导出过程中
     * 1.先创建Invoker,先在RegistryProtocol#refer,
     * 订阅得到Provider端的信息后再到DubboProtocol#refer创建DubboInvoker
     * 饭回到ReferenceConfig#createInvokerForRemote中,合并为ClusterInvoker
     * 2.再创建代理
     */
    private static final String ABSTRACT_INVOKER = "org.apache.dubbo.rpc.protocol.AbstractInvoker";

    @Override
    public String getTrafficType() {
        return PLUGIN_NAME;
    }


    @Override
    protected String getClassPattern() {
        return ABSTRACT_INVOKER;
    }
}
