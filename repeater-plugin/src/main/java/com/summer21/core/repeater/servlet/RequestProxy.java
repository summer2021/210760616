package com.summer21.core.repeater.servlet;

import com.summer21.util.StringUtils;
import com.summer21.util.reflect.ReflectUtils;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.repeater.StringCopier;

/**
 * @author ZT 2021-07-18 17:12
 */
public class RequestProxy implements InvocationHandler {

    private static final String CLASS_NAME = "javax.servlet.http.HttpServletRequest";
    private final Object obj;
    private boolean usingBody;
    private InputStream originServletInputStream;
    private String body;

    private RequestProxy(Object obj) throws Throwable {
        this.obj = obj;
        String getContentType = (String) ReflectUtils.invoke(obj, "getContentType", new Class[0], new Object[0]);
        String applicationJson = "application/json";
        usingBody = !StringUtils.isEmpty(getContentType) && getContentType.contains(applicationJson);
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        if (usingBody){
            try {
                originServletInputStream = (InputStream) ReflectUtils.invoke(obj, "getInputStream", new Class[0], new Object[0]);
                if (originServletInputStream != null) {
                    String ce = (String) ReflectUtils.invoke(obj, "getCharacterEncoding", new Class[0], new Object[0]);
                    if (!StringUtils.isEmpty(ce)) {
                        bufferedReader = new BufferedReader(new InputStreamReader(originServletInputStream, ce));
                    } else {
                        bufferedReader = new BufferedReader(new InputStreamReader(originServletInputStream));
                    }
                    char[] charBuffer = new char[128];
                    int bytesRead;
                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
                }
            } finally {
                if(bufferedReader!=null){
                    bufferedReader.close();
                }
            }
        }
        body = stringBuilder.toString();
    }

    public static Object newRequestProxy(ClassLoader loader, Object obj) throws Throwable {
        InvocationHandler responseProxy = new RequestProxy(obj);
        Class<?> objClass = obj.getClass();
        Class<?> interfaceClass = loader.loadClass(CLASS_NAME);
        return Proxy.newProxyInstance(loader, new Class[]{interfaceClass, StringCopier.class}, responseProxy);
    }


    public BufferedReader getReader() throws Throwable {
        if (usingBody) {
            return new BufferedReader(new InputStreamReader((InputStream) this.getInputStream()));
        } else {
            return (BufferedReader) ReflectUtils.invoke(obj, "getReader", new Class[0], new Object[0]);
        }
    }

    public Object getInputStream() throws Throwable {
        if (usingBody) {
            return ServletInputStreamProxy.newServletInputStreamProxy(obj.getClass().getClassLoader(), originServletInputStream, body);
        } else {
            return ReflectUtils.invoke(obj, "getInputStream", new Class[0], new Object[0]);
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String name = method.getName();
        Class<?>[] parameterTypes = method.getParameterTypes();
        if ("startAsync".equals(name)) {

        } else if ("getInputStream".equals(name)) {
            return getInputStream();
        } else if ("getReader".equals(name)) {
            return getReader();
        } else if("getCopy".equals(name) && parameterTypes.length == 0) {
            return getCopy();
        }
        return method.invoke(obj, args);
    }

    public String getCopy() {
        return body;
    }
}
