package com.summer21.core.repeater;


import java.io.IOException;


/**
 * @author Administrator
 */
public interface TrafficIO {

    /**
     * 写流量
     * @param repeaterTraffic
     * @throws IOException
     */
    void writeTraffic(RepeaterTraffic repeaterTraffic) throws IOException;

    /**
     * TODO 应该加一个参数指定某个流量？
     * 读取一个流量
     * @return 流量记录
     */
    RepeaterTraffic readTraffic();

    /**
     * 读取所有流量
     * @return
     */
    RepeaterTraffic[] readAllTraffic();

    /**
     * 异步保存流量记录
     * @param repeaterTraffic 流量记录
     */
    void asynWriteTraffic(RepeaterTraffic repeaterTraffic);

    /**
     * 异步读取流量记录
     * @return 流量记录
     */
    RepeaterTraffic[] asynReadTraffic();

}
