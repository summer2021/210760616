package com.summer21.core.repeater.servlet;

import com.summer21.util.reflect.ReflectUtils;
import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author ZT 2021-08-18 16:00
 */
public class ServletInputStreamProxy implements MethodHandler {

    private static final String TARGET_CLASS_NAME = "javax.servlet.ServletInputStream";

    private final ByteArrayInputStream byteArrayInputStream;

    private final InputStream originServletInputStream;

    public ServletInputStreamProxy(String body, InputStream originServletInputStream) {
        this.byteArrayInputStream = new ByteArrayInputStream(body.getBytes());
        this.originServletInputStream = originServletInputStream;
    }

    public static Object newServletInputStreamProxy(final ClassLoader loader, InputStream originServletInputStream, String body) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        ProxyFactory.classLoaderProvider = new ProxyFactory.ClassLoaderProvider() {
            @Override
            public ClassLoader get(ProxyFactory pf) {
                return ServletInputStreamProxy.class.getClassLoader();
            }
        };
        ProxyFactory factory = new ProxyFactory();
        factory.setSuperclass(loader.loadClass(TARGET_CLASS_NAME));
        ServletInputStreamProxy servletInputStreamProxy = new ServletInputStreamProxy(body, originServletInputStream);
        return factory.create(new Class<?>[0], new Object[0], servletInputStreamProxy);
    }

    @Override
    public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
        String name = thisMethod.getName();
        Class<?>[] parameterTypes = thisMethod.getParameterTypes();
        try {
            Method method = byteArrayInputStream.getClass().getMethod(name, parameterTypes);
            return method.invoke(byteArrayInputStream, args);
        } catch (NoSuchMethodException ignore) {

        }
        if ("isFinished".equals(name)) {
            return false;
        } else if ("isReady".equals(name)) {
            return false;
        } else if ("setReadListener".equals(name)) {
            return null;
        }
        // 漏网之鱼？
        return ReflectUtils.invoke(originServletInputStream, thisMethod.getName(), thisMethod.getParameterTypes(), args);
    }
}
