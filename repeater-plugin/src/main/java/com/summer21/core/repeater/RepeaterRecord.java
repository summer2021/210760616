package com.summer21.core.repeater;

import java.util.HashMap;
import java.util.Map;

/**
 * 记录入口调用、子调用、以及URL请求等等
 *
 * @author ZT 2021-07-09 23:59
 */
public class RepeaterRecord implements java.io.Serializable {
    /**
     * 用来记录流量的发起参数容如URL等
     */
    private InvocationContext invocationContext;
    /**
     * 入口调用的类加载器
     */
    private final transient ClassLoader loader;
    /**
     * 入口调用的入参
     */
    private final transient Object[] params;
    /**
     * 入口调用的返回值
     */
    private transient Object returnObj;
    /**
     * 入口调用抛出的异常
     */
    private Throwable throwExp;

    private boolean isAfterThrowing;
    private boolean isAfterReturning;

    /**
     * 子调用记录, String。
     * TODO 反序列化时没有再刻意对子调用里面的对象及其类加载器进行还原。全凭PluginClassLoader能否加载到
     */
    private Map<String, SubInvocation> subInvocations = new HashMap<String, SubInvocation>();

    private RepeaterRecord() {
        this(null, null);
    }

    public RepeaterRecord(ClassLoader loader, Object[] params) {
        this.loader = loader;
        this.params = params;
    }

    public InvocationContext getInvocationContext() {
        return invocationContext;
    }

    public void setInvocationContext(InvocationContext invocationContext) {
        this.invocationContext = invocationContext;
    }

    public Map<String, SubInvocation> getSubInvocations() {
        return subInvocations;
    }

    public void setSubInvocations(Map<String, SubInvocation> subInvocations) {
        this.subInvocations = subInvocations;
    }

    public void setReturnObj(Object returnObj) {
        this.returnObj = returnObj;
        this.setAfterThrowing(false);
        this.setAfterReturning(true);
    }

    public void setThrowExp(Throwable throwExp) {
        this.throwExp = throwExp;
        this.setAfterThrowing(true);
        this.setAfterReturning(false);
    }

    public boolean isAfterThrowing() {
        return isAfterThrowing;
    }

    public void setAfterThrowing(boolean afterThrowing) {
        isAfterThrowing = afterThrowing;
    }

    public boolean isAfterReturning() {
        return isAfterReturning;
    }

    public void setAfterReturning(boolean afterReturning) {
        isAfterReturning = afterReturning;
    }

    public ClassLoader getLoader() {
        return loader;
    }

    public Object[] getParams() {
        return params;
    }

    public Object getReturnObj() {
        return returnObj;
    }

    public Throwable getThrowExp() {
        return throwExp;
    }
}
