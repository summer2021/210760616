package com.summer21.core.repeater.servlet;

import com.summer21.core.advisor.ArthasMethod;
import com.summer21.core.repeater.AbstractRepeater;
import com.summer21.core.repeater.InvocationContext;
import com.summer21.core.repeater.RepeaterRecord;
import com.summer21.core.repeater.RepeaterTraffic;
import com.summer21.util.HttpUtils;
import com.summer21.util.matcher.Matcher;
import okhttp3.Response;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.repeater.BytesCopier;
import java.repeater.StringCopier;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;


/**
 * @author ZT 2021-07-15 11:14
 * @see org.springframework.web.servlet.FrameworkServlet#service 这个方法加入了对PATCH的处理
 * @see javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse) (javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)  这个方法没有对PATCH进行处理
 * 但上两者方法都是由{@link javax.servlet.http.HttpServlet#service(javax.servlet.ServletRequest, javax.servlet.ServletResponse)}
 * 这个方法调用的。
 * 为了屏蔽PATCH的差异，所以做监听时，监听最后的那个方法。
 * listener会判断是否为重载方法，重载方法会被跳过
 */
public class ServletRepeater extends AbstractRepeater {

    public static final String PLUGIN_NAME = "Servlet";
    private static final String HTTP_SERVLET = "javax.servlet.http.HttpServlet";
    private static final String METHOD_NAME = "service";

    private static final String SERVLET_REQUEST = "javax.servlet.ServletRequest";
    private static final String SERVLET_RESPONSE = "javax.servlet.ServletResponse";

    private static final String REPEATER_TRAFFIC_ID = "REPEATER-TRAFFIC-ID";

    @Override
    public void trafficWrapper(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args) throws Throwable {
        // 不需要手动cast，因为回写的指令会自动CHECKCAST
        args[0] = RequestProxy.newRequestProxy(loader, args[0]);
        args[1] = ResponseProxy.newResponseProxy(loader, args[1]);
    }

    @Override
    public InvocationContext extractContext(RepeaterRecord repeaterRecord) {
        // TODO 此处和extractRecordId实际上都应该使用反射，因为有可能因为classLoader的问题强转失败.
        Object[] args = repeaterRecord.getParams();
        HttpServletRequest httpServletRequest = (HttpServletRequest) args[0];
        HttpServletResponse httpServletResponse = (HttpServletResponse) args[1];
        // URL
        StringBuffer requestURL = httpServletRequest.getRequestURL();
        // Method
        String method = httpServletRequest.getMethod();
        // header
        Map<String, String> headers = new HashMap<String, String>();
        Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
        while (headerNames != null && headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            String header = httpServletRequest.getHeader(headerName);
            headers.put(headerName, header);
        }
        // parameters  直接使用是否会有问题？
        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
        String contentType = httpServletRequest.getContentType();
        StringCopier stringCopier = (StringCopier) httpServletRequest;
        String body = stringCopier.getCopy();
        // response
        BytesCopier bytesCopier = (BytesCopier) httpServletResponse;

        InvocationContext invocationContext = new InvocationContext();
        ServletInvocationContextWrapper wrapper = new ServletInvocationContextWrapper(invocationContext);
        wrapper.setRequestURL(requestURL.toString());
        wrapper.setMethod(method);
        wrapper.setHeaders(headers);
        wrapper.setParameterMap(parameterMap);
        wrapper.setBody(body);
        wrapper.setContentType(contentType);
        wrapper.setResponse(bytesCopier.getCopy());

        // 只保留关键信息即可，request response实例不保留
        args[0] = null;
        args[1] = null;
        return invocationContext;
    }

    @Override
    protected String getClassPattern() {
        return HTTP_SERVLET;
    }

    @Override
    protected String getExcludeClassPattern() {
        return null;
    }

    @Override
    protected String getMethodPattern() {
        return METHOD_NAME;
    }

    @Override
    protected String[] getMethodArgumentsPatterns() {
        return new String[]{SERVLET_REQUEST, SERVLET_RESPONSE};
    }

    @Override
    public Matcher getRequestMatcher() {
        return null;
    }

    @Override
    public String extractRecordId(Object[] args) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) args[0];
        return httpServletRequest.getHeader(REPEATER_TRAFFIC_ID);
    }

    @Override
    public void repeat(RepeaterTraffic repeaterTraffic) throws Throwable {
        ServletInvocationContextWrapper wrapper = new ServletInvocationContextWrapper(repeaterTraffic.getRepeaterRecord().getInvocationContext());
        String recordId = repeaterTraffic.getSeq();
        // 为回放流量添加头部标识
        Map<String, String> headers = wrapper.getHeaders();
        if (headers == null) {
            headers = new HashMap<String, String>(1);
        }
        headers.put(REPEATER_TRAFFIC_ID, recordId);
        Response response = HttpUtils.executeRequest(wrapper.getRequestURL(), wrapper.getMethod(), headers, wrapper.getParameterMap(), wrapper.getBody(), wrapper.getContentType());
        System.out.println("response: " + response.toString());
        if (response.body() != null) {
            System.out.println("body: " + response.body().string());
        }
    }

    @Override
    public String getTrafficType() {
        return PLUGIN_NAME;
    }
}
