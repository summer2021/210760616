package com.summer21.core.repeater.dubbo;

import com.summer21.core.repeater.InvocationContext;

import java.util.List;
import java.util.Map;

/**
 * Dubbo调用相关的参数等
 *
 * @author ZT 2021-08-28 22:22
 */
public class DubboInvocationContext extends InvocationContext {
    private String protocol;
    private String interfaceName;
    private String methodName;
    private Map<String, String> parameters;
    private String version;
    private String[] parameterTypes;
    private Object[] arguments;

    private List<RegistryConfigSnapshot> registryConfigSnapshots;

    public static class RegistryConfigSnapshot implements java.io.Serializable {
        private String group;
        private String address;
        /**
         * TODO registryProtocol是否不用记录？
         */
        private String registryProtocol;

        public RegistryConfigSnapshot() {
        }

        public RegistryConfigSnapshot(String group, String address, String registryProtocol) {
            this.group = group;
            this.address = address;
            this.registryProtocol = registryProtocol;
        }

        public String getGroup() {
            return group;
        }
        public void setGroup(String group) {
            this.group = group;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getRegistryProtocol() {
            return registryProtocol;
        }

        public void setRegistryProtocol(String registryProtocol) {
            this.registryProtocol = registryProtocol;
        }
    }

    /**
     * 本次RPC调用结果中的异常
     */
    private Throwable exception;
    /**
     * 本次RPC调用的结果
     */
    private Object retValue;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String[] getParameterTypes() {
        return parameterTypes;
    }

    public void setParameterTypes(String[] parameterTypes) {
        this.parameterTypes = parameterTypes;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public void setArguments(Object[] arguments) {
        this.arguments = arguments;
    }

    public Throwable getException() {
        return exception;
    }

    public void setException(Throwable exception) {
        this.exception = exception;
    }

    public Object getRetValue() {
        return retValue;
    }

    public void setRetValue(Object retValue) {
        this.retValue = retValue;
    }

    public List<RegistryConfigSnapshot> getRegistryConfigSnapshots() {
        return registryConfigSnapshots;
    }

    public void setRegistryConfigSnapshots(List<RegistryConfigSnapshot> registryConfigSnapshots) {
        this.registryConfigSnapshots = registryConfigSnapshots;
    }
}
