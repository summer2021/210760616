package com.summer21.core.repeater;


import com.alibaba.oneagent.plugin.Plugin;
import com.summer21.GlobalOptions;
import com.summer21.RepeaterActivator;
import com.summer21.core.advisor.AdviceListener;
import com.summer21.core.advisor.Enhancer;
import com.summer21.core.advisor.InvokeTraceable;
import com.summer21.core.repeater.dubbo.DubboConsumerRepeater;
import com.summer21.core.repeater.dubbo.DubboProviderRepeater;
import com.summer21.core.repeater.servlet.ServletRepeater;
import com.summer21.core.serialize.HessianSerializer;
import com.summer21.core.serialize.Serializer;
import com.summer21.util.Constants;
import com.summer21.util.StringUtils;
import com.summer21.util.affect.EnhancerAffect;
import com.summer21.util.matcher.GroupMatcher;
import com.summer21.util.matcher.Matcher;
import com.summer21.util.matcher.WildcardMatcher;
import com.summer21.util.reflect.ReflectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * todo 是否需要加载已经保存在文件中的流量
 *
 * @author ZT
 * @version 1.0
 */
public class RepeaterCommand {

    /**
     * 支持的插件
     * map{PLUGIN_NAME:Repeater}
     */
    private static final Map<String, Class<? extends Repeater>> repeatersMap = new HashMap<String, Class<? extends Repeater>>();

    static {
        repeatersMap.put(DubboConsumerRepeater.PLUGIN_NAME, DubboConsumerRepeater.class);
        repeatersMap.put(DubboProviderRepeater.PLUGIN_NAME, DubboProviderRepeater.class);
        repeatersMap.put(ServletRepeater.PLUGIN_NAME, ServletRepeater.class);
    }

    public static Serializer serializer = new HessianSerializer();

    private static TrafficIO trafficIO = new DefaultTrafficIO(serializer);

    /**
     * 时间戳加上这个序号为ID
     */
    private static AtomicInteger sequence = new AtomicInteger(1000);

    /**
     * 流量集合， 一次流量录制对应唯一一个id。
     * 时间戳+原子序号，时间戳用来唯一确定一个时刻，原子序号保证并发唯一。
     * 这样即便多次运行插件，原子序号一样，但时间戳肯定不一样，也就ID唯一。
     * TODO 并非线程安全？  现在由LinkedHashMap换成了ConcurrentHashMap
     * 流量是并发，就可能会同时put
     */
    private static final Map<String, RepeaterTraffic> repeaterTrafficMap = new ConcurrentHashMap<String, RepeaterTraffic>();

    private Repeater repeater;

    /**
     * 记录流量的类型，与回放互斥？
     */
    private String recorderType;


    /**
     * 列出所有插件类型，TODO 在help的时候也应该列出
     */
    private boolean isListTypes;

    private String classPattern;
    private String methodPattern;
    private String conditionExpress;
    /**
     * list the TrafficFlow
     */
    private boolean isListTraffic = false;
    private boolean isDeleteAll = false;
    /**
     * index of TrafficFlow
     */
    private Integer index;
    /**
     * expand of TrafficFlow
     */
    private Integer expand = 1;
    /**
     * upper size limit
     */
    private Integer sizeLimit = 10 * 1024 * 1024;
    /**
     * watch the index TrafficFlow
     */
    private String watchExpress = Constants.EMPTY_STRING;
    private String searchExpress = Constants.EMPTY_STRING;

    /**
     * 是否回放流量，与记录互斥？
     */
    private boolean isPlay;

    /**
     * delete the index TimeTunnel
     */
    private boolean isDelete = false;
    private boolean isRegEx = false;
    private int numberOfLimit = 100;
    private int replayTimes = 1;
    private long replayInterval = 1000L;

    /**
     * 录制时表示需要录制的子调用，
     * 回放时表示需要mock的子调用，一定是录制时的子集。
     * eg. demo.repeater.servlet.DemoApplication#subInvoke1 | demo.repeater.servlet.DemoApplication#subInvoke2
     */
    private String subInvoke;

    /**
     * 分割子调用字符串
     */
    private static final String VERTICAL_BAR = "\\|";

    /**
     * 分割子调用字符串
     */
    private static final String NUMBER_SIGN = "#";

    private static final Logger logger = LoggerFactory.getLogger(RepeaterCommand.class);

    public void setListTypes(boolean listTypes) {
        this.isListTypes = listTypes;
    }

    public void setClassPattern(String classPattern) {
        this.classPattern = classPattern;
    }

    public void setMethodPattern(String methodPattern) {
        this.methodPattern = methodPattern;
    }

    public void setConditionExpress(String conditionExpress) {
        this.conditionExpress = conditionExpress;
    }

    public void setRecorder(String recorder) {
        this.recorderType = recorder;
    }

    public void setListTraffic(boolean listTraffic) {
        isListTraffic = listTraffic;
    }

    public void setDeleteAll(boolean deleteAll) {
        isDeleteAll = deleteAll;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public void setExpand(Integer expand) {
        this.expand = expand;
    }

    public void setSizeLimit(Integer sizeLimit) {
        this.sizeLimit = sizeLimit;
    }

    public void setWatchExpress(String watchExpress) {
        this.watchExpress = watchExpress;
    }

    public void setSearchExpress(String searchExpress) {
        this.searchExpress = searchExpress;
    }

    public void setPlay(boolean isPlay) {
        this.isPlay = isPlay;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public void setRegEx(boolean regEx) {
        isRegEx = regEx;
    }

    public void setNumberOfLimit(int numberOfLimit) {
        this.numberOfLimit = numberOfLimit;
    }

    public void setReplayTimes(int replayTimes) {
        this.replayTimes = replayTimes;
    }

    public void setReplayInterval(int replayInterval) {
        this.replayInterval = replayInterval;
    }

    public boolean isListTypes() {
        return isListTypes;
    }

    public boolean isRegEx() {
        return isRegEx;
    }

    public String getMethodPattern() {
        return methodPattern;
    }

    public String getClassPattern() {
        return classPattern;
    }

    public String getConditionExpress() {
        return conditionExpress;
    }

    public int getNumberOfLimit() {
        return numberOfLimit;
    }

    public int getReplayTimes() {
        return replayTimes;
    }

    public long getReplayInterval() {
        return replayInterval;
    }

    private boolean hasWatchExpress() {
        return !StringUtils.isEmpty(watchExpress);
    }

    private boolean hasSearchExpress() {
        return !StringUtils.isEmpty(searchExpress);
    }


    public String getSubInvoke() {
        return subInvoke;
    }

    public void setSubInvoke(String subInvoke) {
        this.subInvoke = subInvoke;
    }

    /**
     * 检查参数是否合法
     */
    private void checkArguments() {
        // 检查d/p参数是否有i参数配套
        if ((isDelete || isPlay) && null == index) {
            throw new IllegalArgumentException("Time fragment index is expected, please type -i to specify");
        }
        // 参数如果合法，根据参数初始化repeaterPlugin
        if (recorderType != null) {
            Class<? extends Repeater> aClass = repeatersMap.get(recorderType);
            if (aClass != null) {
                repeater = (Repeater) ReflectUtils.newInstance(aClass);
            } else {
                throw new IllegalArgumentException("No such recorder traffic type");
            }
        }

        // 一个参数都没有是不行滴
        if (null == index && recorderType == null && !isDeleteAll && StringUtils.isEmpty(watchExpress)
                && !isListTraffic && !isListTypes && StringUtils.isEmpty(searchExpress)) {
            throw new IllegalArgumentException("Argument(s) is/are expected, type 'help rp' to read usage");
        }
    }


    public void process(final Plugin plugin) {
        // 检查参数
        checkArguments();

        if (recorderType != null) {
            // 唯一定位到类，这样修改可能会有并发问题？
            boolean swap = GlobalOptions.isDisableSubClass;
            GlobalOptions.isDisableSubClass = true;
            enhance(plugin);
            GlobalOptions.isDisableSubClass = swap;
        } else if (isPlay) {
            processPlay();
        } else if (isListTraffic) {
//            processListTraffic(plugin);
        } else if (isDeleteAll) {
//            processDeleteAll(process);
        } else if (isDelete) {
//            processDelete(process);
        } else if (hasSearchExpress()) {
//            processSearch(process);
        } else if (index != null) {
            if (hasWatchExpress()) {
//                processWatch(process);
            } else {
//                processShow(process);
            }
        } else if (isListTypes) {
//            processListTypes(plugin);
        }

    }

    /**
     * 子调用的类名匹配
     * @return matchers
     */
    private List<Matcher> getSubInvokeClassNameMatchers(){
        if(StringUtils.isEmpty(subInvoke)){
            return null;
        }
        String[] splits = subInvoke.split(VERTICAL_BAR);
        List<Matcher> matchers = new ArrayList<Matcher>(splits.length);
        for (String split : splits) {
            String className = split.split(NUMBER_SIGN)[0].trim();
            matchers.add(new WildcardMatcher(className));
        }
        return matchers;
    }

    /**
     * 子调用的排除的类名，先设置为null，无排除
     * @return matchers
     */
    private List<Matcher> getSubInvokeClassNameExcludeMatchers(){
        return null;
    }

    /**
     * 子调用的方法名匹配
     * @return matchers
     */
    private List<Matcher> getSubInvokeMethodNameMatchers(){
        if(StringUtils.isEmpty(subInvoke)){
            return null;
        }
        String[] splits = subInvoke.split(VERTICAL_BAR);
        List<Matcher> matchers = new ArrayList<Matcher>(splits.length);
        for (String split : splits) {
            String className = split.split(NUMBER_SIGN)[1].trim();
            matchers.add(new WildcardMatcher(className));
        }
        return matchers;
    }

    /**
     * 包括{@link Repeater#getClassNameMatcher()}的入口调用类和{@link #getSubInvokeClassNameMatchers()}的子调用类
     * @return
     */
    protected Matcher getClassNameMatcher() {
        Matcher trafficClassNameMatcher = repeater.getClassNameMatcher();
        List<Matcher> subInvokeClassNameMatchers = getSubInvokeClassNameMatchers();
        if(subInvokeClassNameMatchers == null){
            return trafficClassNameMatcher;
        }
        GroupMatcher.Or or = new GroupMatcher.Or(subInvokeClassNameMatchers);
        or.add(trafficClassNameMatcher);
        return or;
    }

    protected Matcher getClassNameExcludeMatcher() {
        return repeater.getClassNameExcludeMatcher();
    }

    /**
     * 包括{@link Repeater#getMethodNameMatcher()}的入口调用方法和{@link #getSubInvokeMethodNameMatchers()}的子调用方法
     * @return
     */
    protected Matcher getMethodNameMatcher() {
        Matcher trafficMethodNameMatcher = repeater.getMethodNameMatcher();
        List<Matcher> subInvokeClassNameMatchers = getSubInvokeMethodNameMatchers();
        if(subInvokeClassNameMatchers == null){
            return trafficMethodNameMatcher;
        }
        GroupMatcher.Or or = new GroupMatcher.Or(subInvokeClassNameMatchers);
        or.add(trafficMethodNameMatcher);
        return or;
    }

    protected AdviceListener getAdviceListener(Plugin plugin) {
        return new RepeaterAdviceListener(this, plugin, GlobalOptions.verbose, repeater);
    }


    protected void enhance(Plugin plugin) {
        EnhancerAffect effect = null;
        try {
            AdviceListener listener = getAdviceListener(plugin);
            boolean skipJDKTrace = false;

            Enhancer enhancer = new Enhancer(listener, listener instanceof InvokeTraceable, skipJDKTrace,
                    getClassNameMatcher(), getClassNameExcludeMatcher(), getMethodNameMatcher());
            // 省略 注册通知监听器
//            plugin.register(listener, enhancer);
            effect = enhancer.enhance(RepeaterActivator.getInstance().getInstrumentation());

            if (effect.getThrowable() != null) {
                String msg = "error happens when enhancing class: " + effect.getThrowable().getMessage();
                logger.error(msg);
                return;
            }

            if (effect.cCnt() == 0 || effect.mCnt() == 0) {
                // no class effected
                // might be method code too large
                logger.info("No class or method is affected");

                return;
            }
            //异步执行，在AdviceListener中结束
        } catch (Throwable e) {
            String msg = "error happens when enhancing class: " + e.getMessage();
            logger.error(msg, e);
        } finally {

        }
    }

    private String id(){
        long l = System.currentTimeMillis();
        int indexOfSeq = sequence.getAndIncrement();
        return Long.toString(l) + indexOfSeq;
    }

    String putRepeaterTraffic(RepeaterTraffic repeaterTraffic) {
        String id = id();
        repeaterTrafficMap.put(id, repeaterTraffic);
        return id;
    }

    RepeaterTraffic getRepeaterTraffic(String id) {
        return repeaterTrafficMap.get(id);
    }

    /**
     * 重放指定记录
     */
    private void processPlay() {
        RepeaterTraffic[] repeaterTraffics = trafficIO.readAllTraffic();
        if (repeaterTraffics == null) {
            logger.error("no RepeaterTraffic");
            return;
        }
        for (RepeaterTraffic rt : repeaterTraffics) {
            // 放入缓存中，以便回放时取子调用结果
            String seq = rt.getSeq();
            repeaterTrafficMap.put(seq, rt);
            String trafficType = rt.getTrafficType();
            Class<? extends Repeater> aClass = repeatersMap.get(trafficType);
            if (aClass != null) {
                Repeater repeaterPlugin = (Repeater) ReflectUtils.newInstance(aClass);
                try {
                    repeaterPlugin.repeat(rt);
                } catch (Throwable throwable) {
                    logger.error(throwable.getMessage());
                }
//            process.end();
            } else {
                logger.warn("rp replay failed. Error traffic type: " + trafficType);
//            process.end(-1, "rp replay failed. Error traffic type: " + trafficType);
            }
        }
    }


    public void saveTraffic(RepeaterTraffic repeaterTraffic) {
        try {
            trafficIO.writeTraffic(repeaterTraffic);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
