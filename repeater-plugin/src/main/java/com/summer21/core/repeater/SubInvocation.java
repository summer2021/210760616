package com.summer21.core.repeater;

import com.summer21.core.advisor.AccessPoint;
import com.summer21.core.advisor.Advice;
import com.summer21.core.advisor.ArthasMethod;

/**
 * @author ZT 2021-08-16 17:16
 */
public class SubInvocation implements java.io.Serializable {

    private Object[] params;
    private Object returnObj;
    private Throwable throwExp;

    private boolean isBefore;
    private boolean isThrow;
    private boolean isReturn;

    public static String key(String className, String methodName, String methodDesc) {
        return className+methodName+methodDesc;
    }

    public Object[] getParams() {
        return params;
    }

    public Object getReturnObj() {
        return returnObj;
    }

    public Throwable getThrowExp() {
        return throwExp;
    }

    public boolean isBefore() {
        return isBefore;
    }

    public boolean isThrow() {
        return isThrow;
    }

    public boolean isReturn() {
        return isReturn;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public void setReturnObj(Object returnObj) {
        this.returnObj = returnObj;
    }

    public void setThrowExp(Throwable throwExp) {
        this.throwExp = throwExp;
    }

    public void setBefore(boolean before) {
        isBefore = before;
    }

    public void setThrow(boolean aThrow) {
        isThrow = aThrow;
    }

    public void setReturn(boolean aReturn) {
        isReturn = aReturn;
    }

    private SubInvocation(){}

    private SubInvocation(Object[] params, Object returnObj, Throwable throwExp, boolean isBefore, boolean isThrow, boolean isReturn) {
        this.params = params;
        this.returnObj = returnObj;
        this.throwExp = throwExp;
        this.isBefore = isBefore;
        this.isThrow = isThrow;
        this.isReturn = isReturn;
    }

    public static SubInvocation newForAfterRetuning(
                                             Object[] params,
                                             Object returnObj) {
        return new SubInvocation(
                params,
                returnObj,
                null,
                false,
                false,
                true
        );
    }

    public static SubInvocation newForAfterThrowing(
                                             Object[] params,
                                             Throwable throwExp) {
        return new SubInvocation(
                params,
                null,
                throwExp,
                false,
                true,
                false
        );

    }
}
