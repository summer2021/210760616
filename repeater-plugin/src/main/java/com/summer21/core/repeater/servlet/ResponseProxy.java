package com.summer21.core.repeater.servlet;

import com.summer21.util.reflect.ReflectUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.repeater.BytesCopier;
import java.util.Arrays;

/**
 * @author ZT 2021-07-18 16:49
 */
public class ResponseProxy implements InvocationHandler, BytesCopier {

    private final Object originResponse;

    private OutputStream originServletOutputStream;
    private PrintWriter writer;
    private Object servletOutputStreamCopier;

    private ResponseProxy(Object originResponse) {
        this.originResponse = originResponse;
    }

    public static Object newResponseProxy(ClassLoader loader, Object obj) throws Throwable {
        InvocationHandler invocationHandler = new ResponseProxy(obj);
        Class<?>[] interfaces = obj.getClass().getInterfaces();
        interfaces = Arrays.copyOf(interfaces, interfaces.length + 1);
        interfaces[interfaces.length-1] = BytesCopier.class;
        return Proxy.newProxyInstance(loader, interfaces, invocationHandler);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        if ("getOutputStream".equals(methodName)) {
            return getOutputStream();
        } else if ("getWriter".equals(methodName)) {
            return getWriter();
        } else if ("flushBuffer".equals(methodName)) {
            flushBuffer();
            return null;
        } else if ("getCopy".equals(methodName)) {
            return getCopy();
        }
        return ReflectUtils.invoke(originResponse, method, args);
    }


    private OutputStream getOriginOutputStream() throws Throwable {
        return (OutputStream) ReflectUtils.invoke(originResponse,
                "getOutputStream", new Class[0], new Object[0]);
    }

    private String getCharacterEncoding() throws Throwable {
        return (String) ReflectUtils.invoke(originResponse,
                "getCharacterEncoding", new Class[0], new Object[0]);
    }

    public Object getOutputStream() throws Throwable {
        if (writer != null) {
            throw new IllegalStateException("getWriter() has already been called on this response.");
        }

        if (originServletOutputStream == null) {
            originServletOutputStream = getOriginOutputStream();
            servletOutputStreamCopier = ServletOutputStreamProxy.newServletOutputStreamProxy(originServletOutputStream.getClass().getClassLoader(), originServletOutputStream);
        }
        return servletOutputStreamCopier;
    }

    public PrintWriter getWriter() throws Throwable {
        if (originServletOutputStream != null) {
            throw new IllegalStateException("getOutputStream() has already been called on this response.");
        }
        if (writer == null) {
            OutputStream outputStream = getOriginOutputStream();
            servletOutputStreamCopier = ServletOutputStreamProxy.newServletOutputStreamProxy(outputStream.getClass().getClassLoader(), outputStream);
            writer = new PrintWriter(new OutputStreamWriter((OutputStream) servletOutputStreamCopier, getCharacterEncoding()), true);
        }
        return writer;
    }

    public void flushBuffer() throws IOException {
        if (writer != null) {
            writer.flush();
        } else if (originServletOutputStream != null) {
            ((OutputStream) servletOutputStreamCopier).flush();
        }
    }

    @Override
    public byte[] getCopy() {
        if (servletOutputStreamCopier != null) {
            return ((BytesCopier) servletOutputStreamCopier).getCopy();
        } else {
            return new byte[0];
        }
    }


}
