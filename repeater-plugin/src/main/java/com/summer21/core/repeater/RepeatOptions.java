package com.summer21.core.repeater;

/**
 *
 * @author ZT 2021-07-12 20:31
 */
public class RepeatOptions {

    /**
     * @see com.taobao.arthas.core.command.monitor200.RepeaterCommand#sequence
     */
    private final int sequence;

    private final boolean isRepeatFlow;
    /**
     * 是否进行子调用Mock
     */
    private boolean isMock = true;
    /**
     * 要mock的子调用的行号
     */
    private int[] mockLineNumber;

    public RepeatOptions(int sequence, boolean isRepeatFlow) {
        this.sequence = sequence;
        this.isRepeatFlow = isRepeatFlow;
    }

    // 还有其他可能要添加的什么信息...


    public boolean isRepeatFlow() {
        return isRepeatFlow;
    }

    public boolean isMock() {
        return isMock;
    }

    public void setMock(boolean mock) {
        isMock = mock;
    }

    public int[] getMockLineNumber() {
        return mockLineNumber;
    }

    public void setMockLineNumber(int[] mockLineNumber) {
        this.mockLineNumber = mockLineNumber;
    }

    public int getSequence() {
        return sequence;
    }
}
