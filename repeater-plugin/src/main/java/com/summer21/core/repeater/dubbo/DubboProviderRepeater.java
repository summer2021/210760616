package com.summer21.core.repeater.dubbo;

import com.summer21.core.advisor.ArthasMethod;
import com.summer21.core.repeater.AbstractRepeater;
import com.summer21.core.repeater.InvocationContext;
import com.summer21.core.repeater.RepeaterRecord;
import com.summer21.core.repeater.RepeaterTraffic;
import com.summer21.util.matcher.Matcher;
import com.summer21.util.reflect.ReflectUtils;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.bootstrap.DubboBootstrap;
import org.apache.dubbo.config.utils.ReferenceConfigCache;
import org.apache.dubbo.rpc.RpcContext;
import org.apache.dubbo.rpc.service.GenericService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Administrator 流量的截取点在Filter链的末尾，也就是真实的
 * @version 1.0
 * @date 2021-07-08 16:46
 */
public class DubboProviderRepeater extends AbstractRepeater {


    private static final Logger logger = LoggerFactory.getLogger(DubboProviderRepeater.class);

    public static final String PLUGIN_NAME = "DubboProvider";

    /**
     * 这个invoker中保存的才是真实的服务实现，位于Filter链最后的位置。
     * InvokerWrapper = (DelegateProviderMetaDataInvoker + providerURL)
     * > (DelegateProviderMetaDataInvoker = AbstractProxyInvoker + metadata)
     * > (AbstractProxyInvoker = ref + registryRUL)
     */
    private static final String ABSTRACT_PROXY_INVOKER = "org.apache.dubbo.rpc.protocol.InvokerWrapper";

    private static final String METHOD_NAME = "invoke";

    private static final String REPEATER_TRAFFIC_ID = "REPEATER-TRAFFIC-ID";

    private static ThreadLocal<Object> invokerRef = new ThreadLocal<>();

    @Override
    public void trafficWrapper(ClassLoader loader, Class<?> clazz, ArthasMethod method, Object target, Object[] args) throws Throwable {
        invokerRef.set(target);
    }

    /**
     * 将真实的返回值或者异常解析出来。
     * 无论是返回值或者异常，都是返回的结果
     *
     * @param returnObject Result
     * @return value or exception
     */
    @Override
    protected Object resolveReturnObject(Object returnObject) {
        try {
            boolean hasException = (boolean) ReflectUtils.invoke(returnObject, "hasException");
            if (hasException) {
                return ReflectUtils.invoke(returnObject, "getException");
            } else {
                return ReflectUtils.invoke(returnObject, "getValue");
            }
        } catch (Throwable throwable) {
            return returnObject;
        }
    }

    @Override
    protected Throwable resolveThrowable(Throwable throwable) {
        return throwable;
    }

    @Override
    public InvocationContext extractContext(RepeaterRecord repeaterRecord) {
        Object[] params = repeaterRecord.getParams();
        //  InvokerDelegate extends InvokerWrapper
        Object invoker = invokerRef.get();
        invokerRef.remove();
        Object invocation = params[0];

        try {
            ClassLoader classLoader = invocation.getClass().getClassLoader();
            Object url = ReflectUtils.invoke(invoker, "getUrl");
            @SuppressWarnings("unchecked")
            Map<String, String> parameters = (Map<String, String>) ReflectUtils.invoke(url, "getParameters");
            // protocol
            String protocol = (String) ReflectUtils.invoke(url, "getProtocol");
            // methodName
            String methodName = (String) ReflectUtils.invoke(invocation, "getMethodName");
            // parameters types
            Class<?>[] parameterClass = (Class<?>[]) ReflectUtils.invoke(invocation, "getParameterTypes");
            String[] parameterTypes = new String[parameterClass.length];
            for (int i = 0; i < parameterClass.length; i++) {
                parameterTypes[i] = parameterClass[i].getName();
            }
            // Arguments 序列化
            Object[] arguments = (Object[]) ReflectUtils.invoke(invocation, "getArguments");
            Class<?> pojoUtils = classLoader.loadClass("org.apache.dubbo.common.utils.PojoUtils");
            Method method = pojoUtils.getMethod("generalize", Object[].class);
            arguments = (Object[]) method.invoke(null, new Object[]{arguments});
            // interfaceName
            String interfaceName = ((Class) ReflectUtils.invoke(invoker, "getInterface")).getName();
            DubboInvocationContext dubboInvocationContext = new DubboInvocationContext();
            dubboInvocationContext.setArguments(arguments);
            dubboInvocationContext.setProtocol(protocol);
            dubboInvocationContext.setInterfaceName(interfaceName);
            dubboInvocationContext.setMethodName(methodName);
            dubboInvocationContext.setParameters(parameters);
            dubboInvocationContext.setVersion(parameters.get("version"));
            dubboInvocationContext.setParameterTypes(parameterTypes);

            // get registry protocol and address
            // TODO 注册中心配置一般不会改变，考虑不每次都记录或者记录索引就行，或者由配置决定如何记录。
            Collection registryConfigs;
            try {
                // InvokerDelegate#getInvoker()
                Object delegateProviderMetaDataInvoker = ReflectUtils.invoke(invoker, "getInvoker");
                Object metadata = ReflectUtils.invoke(delegateProviderMetaDataInvoker, "getMetadata");
                registryConfigs = (Collection) ReflectUtils.invoke(metadata, "getRegistries");
            } catch (Throwable ignore) {
                registryConfigs = getRegistryConfigs(classLoader);
            }

            List<DubboInvocationContext.RegistryConfigSnapshot> registryConfigSnapshots =
                    new ArrayList<DubboInvocationContext.RegistryConfigSnapshot>(registryConfigs.size());
            for (Object registryConfig : registryConfigs) {
                String group = (String) ReflectUtils.invoke(registryConfig, "getGroup");
                String address = (String) ReflectUtils.invoke(registryConfig, "getAddress");
                String registryProtocol = (String) ReflectUtils.invoke(registryConfig, "getProtocol");
                registryConfigSnapshots.add(new DubboInvocationContext.RegistryConfigSnapshot(group, address,
                        registryProtocol));
            }
            dubboInvocationContext.setRegistryConfigSnapshots(registryConfigSnapshots);

            return dubboInvocationContext;
        } catch (Throwable throwable) {
            logger.error("extract dubbo invocation context error", throwable);
        }
        return null;
    }

    /**
     * 从ConfigManager获取注册中心的配置。
     *
     * @param classLoader dubbo的classloader
     * @return registryConfigs的collection
     * @throws Throwable
     */
    private Collection getRegistryConfigs(ClassLoader classLoader) throws Throwable {
        Collection registryConfigs;
        try {
            // 2.7.4及其之前的版本
            Class<?> aClass = classLoader.loadClass("org.apache.dubbo.config.context.ConfigManager");
            Method getInstance = aClass.getMethod("getInstance");
            Object configManager = getInstance.invoke(null);
            Map registries = (Map) ReflectUtils.invoke(configManager, "getRegistries");
            registryConfigs = registries.values();
        } catch (NoSuchMethodException exp) {
            // since 2.7.5
            Class<?> aClass = classLoader.loadClass("org.apache.dubbo.rpc.model.ApplicationModel");
            Method getConfigManager = aClass.getMethod("getConfigManager");
            Object configManager = getConfigManager.invoke(null);
            registryConfigs = (Collection) ReflectUtils.invoke(configManager, "getRegistries");
        }
        return registryConfigs;
    }

    @Override
    public Matcher getRequestMatcher() {
        return null;
    }

    @Override
    public String extractRecordId(Object[] args) {
        Object invocation = args[0];
        ClassLoader classLoader = invocation.getClass().getClassLoader();
        String rpcContextName = "org.apache.dubbo.rpc.RpcContext";
        String id = null;
        try {
            Class<?> aClass = classLoader.loadClass(rpcContextName);
            Method getContext = aClass.getMethod("getContext");
            Object context = getContext.invoke(null);

            id = (String) ReflectUtils.invoke(context, "getAttachment", new Class[]{String.class},
                    new Object[]{REPEATER_TRAFFIC_ID});
        } catch (Throwable throwable) {
            logger.error("extract dubbo record id error", throwable);
        }
        return id;
    }

    /**
     * 泛化调用，但由于one-java-agent本身的原因，控制台输出红字，system.err?
     *
     * @param repeaterTraffic 通知点
     * @throws Throwable
     */
    @Override
    public void repeat(RepeaterTraffic repeaterTraffic) throws Throwable {
        DubboInvocationContext invocationContext = (DubboInvocationContext) repeaterTraffic.getRepeaterRecord().getInvocationContext();
        // TODO 类加载器问题 <scope>provided</scope> 自己加载。 应该从某个类加载器已加载的dubbo中拿，然后反射进行下列操作？
        ReferenceConfig<GenericService> reference = new ReferenceConfig<>();
        reference.setInterface(invocationContext.getInterfaceName());
        // reference.setGeneric("true");
        reference.setGeneric(true);
        reference.setProtocol(invocationContext.getProtocol());
        reference.setVersion(invocationContext.getVersion());

        // Registry configs
        List<DubboInvocationContext.RegistryConfigSnapshot> registryConfigSnapshots = invocationContext.getRegistryConfigSnapshots();
        List<RegistryConfig> registryConfigs = new ArrayList<RegistryConfig>(registryConfigSnapshots.size());
        for (DubboInvocationContext.RegistryConfigSnapshot registryConfigSnapshot : registryConfigSnapshots) {
            String address = registryConfigSnapshot.getAddress();
            String group = registryConfigSnapshot.getGroup();
            String registryProtocol = registryConfigSnapshot.getRegistryProtocol();
            RegistryConfig registryConfig = new RegistryConfig();
            registryConfig.setAddress(address);
            registryConfig.setGroup(group);
            registryConfig.setProtocol(registryProtocol);
            registryConfigs.add(registryConfig);
        }
        reference.setRegistries(registryConfigs);

        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("one-java-agent-repeater");
        try {
            // @since 2.7.5
            DubboBootstrap bootstrap = DubboBootstrap.getInstance();
            bootstrap.application(applicationConfig)
                    .reference(reference)
                    .start();
        } catch (Throwable throwable) {
            // 2.7.4及其之前
            applicationConfig = new ApplicationConfig();
            reference.setApplication(applicationConfig);
        }
        // traffic id
        RpcContext.getContext().setAttachment(REPEATER_TRAFFIC_ID, repeaterTraffic.getSeq());
        // generic invoke
        GenericService genericService;
        try {
            genericService = (GenericService) ReferenceConfigCache.getCache().get(reference);
        } catch (Throwable throwable) {
            genericService = reference.get();
        }
        Object genericInvokeResult = genericService.$invoke(invocationContext.getMethodName(), invocationContext.getParameterTypes(),
                invocationContext.getArguments());
        System.out.println(genericInvokeResult);
    }

    @Override
    public String getTrafficType() {
        return PLUGIN_NAME;
    }

    @Override
    protected String getClassPattern() {
        return ABSTRACT_PROXY_INVOKER;
    }

    @Override
    protected String getExcludeClassPattern() {
        return null;
    }

    @Override
    protected String getMethodPattern() {
        return METHOD_NAME;
    }

    @Override
    protected String[] getMethodArgumentsPatterns() {
        return new String[]{"org.apache.dubbo.rpc.Invocation"};
    }
}
