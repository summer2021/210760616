package com.summer21.core.serialize;

import com.caucho.hessian.io.*;
import com.summer21.core.repeater.ClassLoaderMap;
import com.summer21.core.repeater.RepeaterTraffic;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author ZT 2021-08-18 10:57
 */
public class HessianSerializer extends AbstractSerializer {

    /**
     * String, classloader
     */
    private static Map<String, SerializerFactory> cached = new ConcurrentHashMap<String, SerializerFactory>();

    @Override
    protected byte[] serializeObject(Object object) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        Hessian2Output output = new Hessian2Output(os);
        // 设置之后可以避免每次都new一个，提升性能
        output.setSerializerFactory(getFactory(object.getClass().getClassLoader()));
        output.writeObject(object);
        output.flush();
        return os.toByteArray();
    }

    @Override
    protected Object deserializeFrom(byte[] bytes, Class clazz) throws IOException {
//        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        Hessian2Input input = new Hessian2Input(new ByteArrayInputStream(bytes));
        input.setSerializerFactory(getFactory(clazz.getClassLoader()));
        return input.readObject(clazz);
    }

    @Override
    public byte[] asynSerialize(RepeaterTraffic repeaterTraffic) {
        return new byte[0];
    }

    @Override
    public RepeaterTraffic asynDeserialize(byte[] bytes) {
        return null;
    }

    private SerializerFactory getFactory(ClassLoader classLoader) {
        String key = ClassLoaderMap.key(classLoader);
        SerializerFactory factory = cached.get(key);
        if (factory == null) {
            if(classLoader == null){
                factory = new SerializerFactory();
            } else{
                factory = new SerializerFactory(classLoader);
            }
            factory.setAllowNonSerializable(true);
//            registerCustomFactory(factory);
            cached.put(key, factory);
        }
        return factory;
    }
}
