package com.summer21.core.serialize;

import com.alibaba.fastjson.JSON;
import com.summer21.core.repeater.RepeaterTraffic;

/**
 * FastJSON序列化的时候存会有一些问题
 * {@link com.taobao.arthas.core.command.monitor200.repeater.RepeaterRecord}
 * TODO 异步序列化
 *
 * @author ZT 2021-07-13 23:11
 */
public class DefaultSerializer extends AbstractSerializer {

    @Override
    protected byte[] serializeObject(Object object) {
        return JSON.toJSONBytes(object);
    }

    @Override
    protected Object deserializeFrom(byte[] bytes, Class clazz) {
        return JSON.parseObject(bytes, clazz);
    }

    @Override
    public final byte[] asynSerialize(RepeaterTraffic repeaterTraffic) {
        return new byte[0];
    }

    @Override
    public final RepeaterTraffic asynDeserialize(byte[] bytes) {
        return null;
    }


}
