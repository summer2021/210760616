package com.summer21.core.serialize;

import com.summer21.core.repeater.ClassLoaderMap;

/**
 * @author ZT 2021-08-13 12:53
 */
public class ObjectBytesWrapper {
    private String classLoaderName;
    private String className;
    private byte[] objectByte;

    private ObjectBytesWrapper(){}

    private ObjectBytesWrapper(String classLoaderName, String className, byte[] objectByte) {
        this.classLoaderName = classLoaderName;
        this.className = className;
        this.objectByte = objectByte;
    }

    public static ObjectBytesWrapper newObjectBytesWrapper(Object object, byte[] objectByte){
        Class<?> aClass = object.getClass();
        String className = aClass.getName();
        ClassLoader classLoader = aClass.getClassLoader();
        String classLoaderName = ClassLoaderMap.key(classLoader);
        return new ObjectBytesWrapper(classLoaderName, className, objectByte);
    }

    public String getClassLoaderName() {
        return classLoaderName;
    }

    public void setClassLoaderName(String classLoaderName) {
        this.classLoaderName = classLoaderName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public byte[] getObjectByte() {
        return objectByte;
    }

    public void setObjectByte(byte[] objectByte) {
        this.objectByte = objectByte;
    }
}
