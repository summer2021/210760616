package com.summer21.core.serialize;

import com.summer21.core.repeater.ClassLoaderMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * TODO 添加使用Hessian序列化，可实现对子类的序列化
 * 拥有还原classLoader的能力
 *
 * @author ZT 2021-08-13 12:51
 */
public abstract class AbstractSerializer implements Serializer {

    private static final Logger logger = LoggerFactory.getLogger(AbstractSerializer.class);


    /**
     * 底层序列化对象
     *
     * @param object object
     * @return 序列化后的字节数组
     */
    protected abstract byte[] serializeObject(Object object) throws IOException;

    /**
     * 底层反序列化
     *
     * @param bytes bytes
     * @return 反序列化后的Object
     */
    protected abstract Object deserializeFrom(byte[] bytes, Class clazz) throws IOException;


    /**
     * TODO 处理null
     *
     * @param object 流量记录
     * @return
     */
    @Override
    public final byte[] serialize(Object object) {
        byte[] bytes = new byte[0];
        try {
            bytes = serializeObject(object);
        } catch (IOException e) {
            logger.error("serialize error", e);
        }
        ObjectBytesWrapper objectBytesWrapper = ObjectBytesWrapper.newObjectBytesWrapper(object, bytes);
        try {
            return serializeObject(objectBytesWrapper);
        } catch (IOException e) {
            logger.error("serialize error", e);
            return null;
        }
    }

    @Override
    public final Object deserialize(byte[] bytes) {
        ObjectBytesWrapper objectBytesWrapper = null;
        try {
            objectBytesWrapper = (ObjectBytesWrapper) deserializeFrom(bytes, ObjectBytesWrapper.class);
            String classLoaderName = objectBytesWrapper.getClassLoaderName();
            ClassLoader classLoader = ClassLoaderMap.get(classLoaderName);
            Class<?> aClass = classLoader.loadClass(objectBytesWrapper.getClassName());
            return deserializeFrom(objectBytesWrapper.getObjectByte(), aClass);
        } catch (ClassNotFoundException e) {
            logger.error("load class err: ", e);
            return null;
        } catch (IOException e) {
            logger.error("deserialize error", e);
            return null;
        }
    }
}
