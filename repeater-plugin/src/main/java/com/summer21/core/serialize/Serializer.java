package com.summer21.core.serialize;


import com.summer21.core.repeater.RepeaterTraffic;

/**
 * @author Administrator
 */
public interface Serializer {

    /**
     * 非异步序列化
     * TODO 序列化的时候，classLoader如何序列化？
     *   可考虑保存classLoader的名称，反序列化时再根据名称
     * @param object 流量记录
     * @return 流量记录序列化之后的字节
     */
    byte[] serialize(Object object);

    /**
     * TODO 反序列化时，注意RepeaterTraffic中的return和throw可能是其他类加载器加载的
     *  也就是classLoader的问题
     * @param bytes 流量记录序列化之后的字节
     * @return 流量记录
     */
    Object deserialize(byte[] bytes);

    /**
     * 异步序列化
     * @param repeaterTraffic  流量记录
     * @return 序列化之后的字节
     */
    byte[] asynSerialize(RepeaterTraffic repeaterTraffic);

    /**
     * 异步反序列化
     * @param bytes 流量记录序列化之后的字节
     * @return 流量记录
     */
    RepeaterTraffic asynDeserialize(byte[] bytes);
}
