package com.activator.test;

import com.alibaba.oneagent.plugin.PluginActivator;
import com.alibaba.oneagent.plugin.PluginContext;
// this.getClass().getClassLoader() -> PluginClassLoader
// getParent() -> AppClassLoader 父子关系。如果没有打破双亲委派，子可见父加载器所加载的内容
// 如果有继承关系，子加载器加载的类型可以强转为父加载器所加载的类型
public class DemoActivator implements PluginActivator {

    @Override
    public boolean enabled(PluginContext context) {
        System.out.println("enabled " + this.getClass().getName());
        return true;
    }

    @Override
    public void init(PluginContext context) throws Exception {
        System.out.println("init " + this.getClass().getName());
    }

    @Override
    public void start(PluginContext context) throws Exception {
        System.out.println("start " + this.getClass().getName());
    }

    @Override
    public void stop(PluginContext context) throws Exception {
        System.out.println("stop " + this.getClass().getName());
    }

}
