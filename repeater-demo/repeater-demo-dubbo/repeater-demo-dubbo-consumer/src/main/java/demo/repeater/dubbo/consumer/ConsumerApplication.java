package demo.repeater.dubbo.consumer;

import demo.repeater.dubbo.api.UserRpcService;
import demo.repeater.dubbo.dto.UserDTO;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class ConsumerApplication {

    @Reference(version = "${dubbo.consumer.UserRpcService.version}")
    private UserRpcService userRpcService;
    private final Logger consumerApplicationLogger = LoggerFactory.getLogger(getClass());


    public static void main(String[] args) {
        // 启动 Spring Boot 应用
        ConfigurableApplicationContext context = SpringApplication.run(ConsumerApplication.class, args);
    }

    @ResponseBody
    @GetMapping("/hello")
    public UserDTO hello(@RequestParam(name = "id", defaultValue = "1") int id) {
        UserDTO user = userRpcService.get(id);
        consumerApplicationLogger.info("[run][发起一次 Dubbo RPC 请求，获得用户为({})", user);
        return user;
    }

}
