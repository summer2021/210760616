package demo.repeater.dubbo.service;

import demo.repeater.dubbo.api.UserRpcService;
import demo.repeater.dubbo.dto.UserDTO;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author Administrator
 */
@Service(version = "${dubbo.provider.UserRpcService.version}")
public class UserRpcServiceImpl implements UserRpcService {

    @Override
    public UserDTO get(Integer id) {
        return new UserDTO().setId(id)
                .setName("没有昵称：" + id)
                .setGender(id % 2 + 1);
    }

}
