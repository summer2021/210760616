package demo.repeater.servlet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

/**
 * debug时VM options:
 * -agentlib:jdwp=transport=dt_socket,address=5005,server=y,suspend=y -javaagent:${agent-path}
 * 例如${agent-path}为C:\Users\Administrator\oneagent\core\oneagent@0.0.2-SNAPSHOT\one-java-agent.jar
 * 然后remotedebug attach到本机5005
 */
@SpringBootApplication
@RestController
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    private static int num1 = 0;
    private static int num2 = 0;

    public static int subInvoke1() {
        System.err.println("sub invoke1");
        num1++;
        return num1;
    }

    public Customer newCustomer(String name) {
        System.err.println("newCustomer");
        Customer customer = new Customer();
        customer.setName(name);
        return customer;
    }

    @GetMapping("/hello")
    public Customer hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        subInvoke1();
        return newCustomer(name);
    }

    @PostMapping("/hello")
    public String writeCnt(@RequestBody Customer customer) {
        subInvoke1();
        return String.format("Hello %s!", customer.getName());
    }

}
