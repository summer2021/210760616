package demo.repeater.servlet;

/**
 * @author ZT 2021-08-18 14:37
 */
public class Customer {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
