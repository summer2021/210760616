package java.repeater;

/**
 *
 * @author ZT 2021-08-19 10:43
 */
public interface StringCopier {

    /**
     * 获取copy的字符串
     * @return copy的字符串
     */
    String getCopy();
}
