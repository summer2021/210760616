package java.repeater;

/**
 *
 * @author ZT 2021-08-06 10:43
 */
public interface BytesCopier {
    /**
     * 获取copy的字节数组
     * @return copy的字节数组
     */
    byte[] getCopy();
}
